# USB Redirector for Linux

## Сборка пакета с помощью checkinstall

[Скачать исходный код](http://www.incentivespro.com/downloads.html#usb-redirector-linux)

***Сборку пакета производить на тестовой машине, дабы не засрать сервер***

Уснановка необходимых пакетов для сборки

```apt-get install linux-headers-amd64 checkinstall```

Скачивание, распаковка и подготовка исходников

```
wget http://www.incentivespro.com/usb-redirector-linux-<arch>.tar.gz
tar -xf usb-redirector-linux-<arch>.tar.gz
mv usb-redirector-linux-<arch> usb-redirector-linux-<version>
cd usb-redirector-linux-<version>
```

При сборке ckeckinstall может выдать ошибку:

```
***  Installation failed!
??? Cannot create directory /usr/local/usb-redirector/bin
make: *** [all] Error 4
```

Чтобы её пофиксить необходимо создать директорию

```mkdir -p /usr/local/usb-redirector/bin```

Далее командой  ```checkinstall --exclude=/etc/rc0.d,/etc/rc1.d,/etc/rc2.d,/etc/rc3.d,/etc/rc4.d,/etc/rc5.d,/etc/rc6.d,/etc/rcS.d ./installer.sh install``` собрать пакет
